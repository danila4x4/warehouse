@extends('layouts.app', ['title' => __('Check Products')])

@section('content')
    @include('users.partials.header', [
    'title' => __('Hello') . ' '. auth()->user()->name,
    'description' => __('This is for check Products. If you see errors you can cancel your import or continuous.'),
    'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Check Products') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if($confirm['errorData'])
                                <div class="table">
                                    <table class="table">
                                        <tr class="bg-danger text-light">
                                            <th colspan="2">Error! Cannot recognize data. Please, recheck the lines</th>
                                        </tr>
                                        @foreach($confirm['errorData'] as $key => $item)
                                            <tr>
                                                <td>{!! $key+1 !!}</td>
                                                <td>{!! $item !!}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @else
                                <div class="bg-info mb-1 p-3 text-light">No Errors. Go ahead!</div>
                            @endif

                            @if($confirm['confirm'])
                                <form action="{!! route('import.store') !!}" method="post">
                                    {!! csrf_field() !!}
                                    <input  class="form-control"type="hidden" name="data" value="{!! $confirm['data'] !!}" required/>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr class="bg-success text-light">
                                                <th>Date</th>
                                                <th>Product</th>
                                                <th>Serial</th>
                                                <th>In/Out</th>
                                                <th>Count of Blanks</th>
                                                <th>Message</th>
                                            </tr>
                                            @foreach($confirm['confirm'] as $item)
                                                <tr>
                                                    <td>{{\Carbon\Carbon::parse($item['data'])->format('d/m/Y')}}</td>
                                                    <td>{{$item['code']}}</td>
                                                    <td>{{$item['serial']}}</td>
                                                    <td>{!! $item['in_out'] !!}</td>
                                                    <td>{{$item['blanks']}}</td>
                                                    <td>{!! $item['message'] !!}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <input class="btn btn-info btn m-auto" type="submit" value="Add">
                                    <a class="btn btn-danger m2"  href="{!! route('import.data') !!}">Cancel</a>
                                </form>
                            @else
                                <div class="bg-danger mb-1 p-3 text-light">NO DATA TO IMPORT</div>
                                <br /><a class="btn btn-danger m-2" href="{!! route('import.data') !!}">Back</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush

