@extends('layouts.app', ['title' => __('Add Blank')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Here you can add a new blank.'),
        'class' => 'col-lg-7'
    ])



    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            @if($blank)
                                <h3 class="col-12 mb-0">{{ __('Edit the Blank #') }} {{$blank->id}}</h3>
                            @else
                                <h3 class="col-12 mb-0">{{ __('Add a New Blank') }}</h3>
                            @endif
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{!! route('blank.add') !!}" method="post">
                            @csrf
                            <input type="hidden" name="blank_id" value="{{$blank->id ?? 0}}" />
                            @method('put')
                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="code" placeholder="product code" value="{{$blank->code ?? old('code')}}" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="description" placeholder="description" value="{{$blank->description ?? old('description')}}" />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="number" min="0" step="1" name="count" placeholder="number of blanks" value="{{$blank->count ?? old('count')}}" />
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                    @foreach($products as $product)
                                        <div class="col-lg-3 col-md-4 col-sm-6">
                                            <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <input type="checkbox" name="products[{{$product->id}}]" class="add_product"  data-product="{{$product->id ?? ''}}" id="{{$product->code ?? ''}}" @if($blank && in_array($product->id, $blank->Products->pluck('id')->toArray())) checked @endif
                                                            @if($blank)
                                                                @if (!in_array($product->id, $blank->Products->pluck('id')->toArray()) && $product->blank_id) disabled @endif
                                                            @else
                                                                @if ($product->blank_id) disabled @endif
                                                            @endif
                                                        value="1" aria-label="Checkbox for following text input">
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control pl-2" value="{{$product->code}}" aria-label="Text input with checkbox" readonly>
                                            </div>
                                        </div>
                                    @endforeach
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-info form-control-alternative" type="submit" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @if($blank)
                <div class="col-xl-4">
                    <div class="card bg-secondary shadow">
                        <div class="card-header border-0">
                            <div class="row align-items-center">
                                <div class="col">
                                    <h3 class="mb-0">Log Adding Count</h3>
                                </div>
                            </div>
                        </div>
                        <div class="dropdown float-right">
                            <a class="btn btn-sm btn-icon-only text-danger float-right" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <a class="dropdown-item text-danger" href="{{ route('blank.delete', $blank->id) }}" onclick="return confirm_delete()" >You are going to delete this blank and all data!!!</a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <!-- Projects table -->
                            <table class="table align-items-center table-flush">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">Add count</th>
                                    <th scope="col">Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($blank->Log as $item)
                                        <tr>
                                            <th scope="row">{{$item->add}}</th>
                                            <td><div class="d-flex align-items-center">{{\Illuminate\Support\Carbon::parse($item->created_at)->format('d/m/Y')}}</div></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script>
        $('document').ready(function () {
            $('.add_product').change(function () {
                var product_id = $(this).data('product');
                var blank_id = "{{$blank->id ?? 0}}";
                var value = $(this).prop('checked') === true ? 1 : 0;
                var _token = "{{ csrf_token() }}";
                if (blank_id) {
                    $.ajax({
                        url: "{{ secure_url('ajax-blank-product') }}",
                        type: "POST",
                        data: {product_id: product_id, blank_id: blank_id, value: value, _token: _token},
                        success: function (data) {}
                    });
                }
            })
        });
        function confirm_delete() {
            return confirm('This is the last action, after that - boom!!! Are you sure delete this Blank?');
        }
    </script>
@endpush