@extends('layouts.app', ['title' => __('Imported Data')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is Imported Area. Yo ucan find all entries, which were imported in Data Base.'),
        'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('History Import Data') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Date</th>
                                    <th>Data</th>
                                </tr>
                                @foreach($import_data as $item)
                                    <tr>
                                        <td>
                                            <b>{!! \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i') !!}</b>
                                        </td>
                                        <td>{!! $item->data !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination">
                            {!! $import_data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush

