@extends('layouts.app', ['title' => __('Import Data')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('This is Import Area.'),
        'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1 mb-10">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Import data') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('import.confirm') }}" autocomplete="off">
                            @csrf

                            <h6 class="heading-small text-muted mb-4">{{ __('Important! Example: 11.03.2020 IN L3D1-0557 L3P-0179 OUT L3D1-0548') }}</h6>

                            @if (session('status'))
                                <div class="alert alert-default alert-dismissible fade show" role="alert">
                                    {!! session('status') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="form-group">
                                <textarea class="form-control" rows="13" name="data" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-outline-default">Check</button>
                        </form>

                    </div>
                </div>
            </div>

            <div class="col-xl-4 order-xl-1 mb-10">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Add In / Out product') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">

                        @if (session('statusProduct'))
                            <div class="alert alert-default alert-dismissible fade show" role="alert">
                                {!! session('statusProduct') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <form action="{!! route('import.warehouse-add') !!}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="product_id">Product</label>
                                <select class="form-control m-10" name="product_id" required>
                                    <option value="">Product</option>
                                    @foreach($products as $product)
                                        <option value="{!! $product->id !!}">{!! $product->code !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="serial">Serial Number</label>
                                <input type="text" class="form-control m-10"  name="serial" placeholder="serial number" />
                            </div>
                            <div class="form-group">
                                <label for="in_out">IN or OUT</label>
                                <select class="form-control m-10" name="in_out" required>
                                    <option value="" disabled="disabled">choose IN or OUT</option>
                                    <option value="1">IN</option>
                                    <option value="0">OUT</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="date" class="form-control m-10"  name="date" value="{{ \Carbon\Carbon::today()->format('Y-m-d') }}" required />
                            </div>
                            <button type="submit" class="btn btn-outline-default">Check</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">{{ __('Last imported items') }}</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('import.last') }}" class="btn btn-sm btn-primary">All Data</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Product</th>
                                    <th>Serial</th>
                                    <th>In/Out</th>
                                    <th>Date</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($warehouse_data as $item)
                                    <tr>
                                        <td><a class="btn btn-outline-secondary border-0" href="{!! secure_url('product-details/'.$item->product_id) !!}">{!! $item->Product->code !!}</a></td>
                                        <td>{!! $item->serial !!}</td>
                                        <td>@if($item->in_out == 1) <span class="text-success">IN</span> @else <span class="text-info">OUT</span> @endif</td>
                                        <td>
                                            <b>{!! \Carbon\Carbon::parse($item->date)->format('d/m/Y') !!}</b>
                                        </td>
                                        <td>
                                            {!! \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i') !!}
                                        </td>
                                        <td><a class="btn btn-danger btn-sm" href="{!! secure_url('import-delete/'.$item->id) !!}" onclick="return confirm('DELETE!!! ARE YOU SURE?')" >Delete</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush











{{--@extends('layouts.master')--}}

{{--<div class="content">--}}
{{--    <div class="title m-b-md">--}}
{{--        Warehose--}}
{{--    </div>--}}

{{--    <h1>Import data</h1>--}}
{{--    <small>Important! Example: 11.03.2020 IN L3D1-0557 L3P-0179 OUT L3D1-0548</small>--}}
{{--    <div class="links">--}}
{{--        <form action="{!! url('confirm') !!}" method="post">--}}
{{--            {!! csrf_field() !!}--}}
{{--            <input type="text" name="data" placeholder="Insert Data" required/>--}}
{{--            <textarea rows="10" name="data" cols="50" required></textarea>--}}
{{--            <div><input type="submit" value="Check"></div>--}}
{{--        </form>--}}
{{--    </div>--}}
{{--    <hr/>--}}
{{--    @if($products)--}}
{{--    <div class="links">--}}
{{--        Add manual--}}
{{--        <form action="{!! url('warehouse-add') !!}" method="post">--}}
{{--            {!! csrf_field() !!}--}}
{{--            <select name="product_id" required />--}}
{{--                <option value="">Product</option>--}}
{{--                @foreach($products as $product)--}}
{{--                    <option value="{!! $product->id !!}">{!! $product->code !!}</option>--}}
{{--                @endforeach--}}
{{--            </select>--}}
{{--           <input type="text" name="serial" placeholder="serial number" />--}}
{{--            <select name="in_out" required />--}}
{{--                <option value="" disabled="disabled">choose IN or OUT</option>--}}
{{--                <option value="1">IN</option>--}}
{{--                <option value="0">OUT</option>--}}
{{--            </select>--}}
{{--            <input type="date" name="date" value="{{ \Carbon\Carbon::today()->format('Y-m-d') }}" required />--}}
{{--            <input type="submit" value="Check">--}}
{{--        </form>--}}
{{--    </div>--}}
{{--    <hr/>--}}
{{--    @endif--}}
{{--    <div  class="links">--}}
{{--        @if(isset($confirm))--}}

{{--            @if($confirm['confirm'])--}}
{{--                <form action="{!! url('import') !!}" method="post">--}}
{{--                    {!! csrf_field() !!}--}}
{{--                    <input type="hidden" name="data" value="{!! $confirm['data'] !!}" required/>--}}
{{--                    <table style="width: 50%; margin: 50px; text-align: center;">--}}
{{--                    <tr>--}}
{{--                        <th>Date</th>--}}
{{--                        <th>Product</th>--}}
{{--                        <th>Serial</th>--}}
{{--                        <th>In/Out</th>--}}
{{--                    </tr>--}}
{{--                        @foreach($confirm['confirm'] as $item)--}}
{{--                            <tr>--}}
{{--                                <td>{!! \Carbon\Carbon::parse($item['data'])->format('d/m/Y') !!}</td>--}}
{{--                                <td>{!! $item['code'] !!}</td>--}}
{{--                                <td>{!! $item['serial'] !!}</td>--}}
{{--                                <td>{!! $item['in_out'] !!}</td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                    </table>--}}
{{--                    <input type="submit" value="Add"> <a href="{!! url('/') !!}">Cancel</a>--}}
{{--                </form>--}}
{{--            @else--}}
{{--                NO DATA TO IMPORT--}}
{{--                <br /><a href="{!! url('/') !!}">Back</a>--}}
{{--            @endif--}}

{{--        @endif--}}
{{--    </div>--}}
{{--    <div  class="links">--}}
{{--        @if($products)--}}
{{--            <form action="{!! url('product-update') !!}" method="post">--}}
{{--                {!! csrf_field() !!}--}}
{{--                <table style="width: 90%; margin: 50px">--}}
{{--                    <tr>--}}
{{--                        <th>Updated</th>--}}
{{--                        <th>Code</th>--}}
{{--                        <th>Count</th>--}}
{{--                        <th>Volume</th>--}}
{{--                        <th>Manufacturer</th>--}}
{{--                        <th>Description</th>--}}
{{--                        <th>Material</th>--}}
{{--                        <th>Wholesale</th>--}}
{{--                        <th>Retail</th>--}}
{{--                        <th>Action</th>--}}
{{--                    </tr>--}}
{{--                    @foreach($products as $product)--}}
{{--                        <tr @if($product->changed == 1) bgcolor="#FF0000" @endif >--}}
{{--                            <td><input type="text" name="data[{!! $product->id !!}][updated_at]" value="{!! \Carbon\Carbon::parse($product->updated_at)->format('d/m/Y') !!}" disabled /></td>--}}
{{--                            <td><input type="text" name="data[{!! $product->id !!}][code]" value="{!! $product->code !!}" required /></td>--}}
{{--                            <td><input type="number" min="0" name="data[{!! $product->id !!}][count]" value="{!! $product->count !!}" required/></td>--}}
{{--                            <td><input type="text" name="data[{!! $product->id !!}][volume]" value="{!! $product->volume !!}" /></td>--}}
{{--                            <td><input type="text" name="data[{!! $product->id !!}][manufacturer]" value="{!! $product->manufacturer !!}" /></td>--}}
{{--                            <td><input type="text" name="data[{!! $product->id !!}][description]" value="{!! $product->description !!}" /></td>--}}
{{--                            <td><input type="text" name="data[{!! $product->id !!}][material]" value="{!! $product->material !!}" /></td>--}}
{{--                            <td><input type="number" min="0" step="0.01" name="data[{!! $product->id !!}][price_wholesale]" value="{!! $product->price_wholesale !!}" /></td>--}}
{{--                            <td><input type="number" min="0" step="0.01" name="data[{!! $product->id !!}][price_retail]" value="{!! $product->price_retail !!}" /></td>--}}
{{--                            <td><a href="{!! url('product-details/'.$product->id) !!} target="_blank">Details</a></td>--}}
{{--                        </tr>--}}
{{--                    @endforeach--}}
{{--                </table>--}}
{{--                <input type="submit" value="Update"> <input type="submit" name="download" value="Download Excel">--}}
{{--            </form>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--    <hr/>--}}
{{--    <div  class="links">--}}
{{--        New Product:--}}
{{--        <form action="{!! url('product-add') !!}" method="post">--}}
{{--            {!! csrf_field() !!}--}}
{{--            <input type="text" name="code" placeholder="product code" required/>--}}
{{--            <input type="text" name="manufacturer" placeholder="manufacturer" />--}}
{{--            <input type="text" name="description" placeholder="description" />--}}
{{--            <input type="text" name="volume" placeholder="volume" />--}}
{{--            <input type="text" name="material" placeholder="material" />--}}
{{--            <input type="number" min="0" step="0.01" name="price_wholesale" placeholder="price wholesale" />--}}
{{--            <input type="number" min="0" step="0.01" name="price_retail" placeholder="price retail" />--}}
{{--            <input type="submit" value="Add">--}}
{{--        </form>--}}
{{--    </div>--}}
{{--    <div  class="links">--}}
{{--        @if(isset($import_data) && $import_data)--}}
{{--            <hr/>--}}
{{--            IMPORTED:--}}
{{--            <table style="width: 90%; margin: 20px">--}}
{{--                @foreach($import_data as $item)--}}
{{--                    <tr>--}}
{{--                        <td>--}}
{{--                            <br/>--}}
{{--                            <b>{!! \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i') !!}</b>--}}
{{--                            <br/>--}}
{{--                            {!! $item->data !!}--}}
{{--                            <hr>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                @endforeach--}}
{{--            </table>--}}
{{--        @endif--}}
{{--    </div>--}}

{{--</div>--}}
