@extends('layouts.app')

@section('content')
    @include('layouts.headers.cards')
    
    <div class="container-fluid mt--7">
        <div class="row mt-5">
            <div class="col-xl-8 mb-5 mb-xl-0">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">{{ __('Warehouse') }}</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('import.data') }}" class="btn btn-sm btn-primary">Import</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Blanks</th>
                                    <th>T.Up</th>
                                    <th scope="col">Items</th>
                                    <th scope="col">Last No</th>
                                    <th scope="col">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $item)
                                    <tr @if($item->actionIn || $item->actionOut)class="bg-light" @endif>
                                        <th scope="row"><a href="{{route('product.details', $item->id)}}">{{$item->code}}</a></th>
                                        <td>@if($item->Blank){{$item->Blank->count}} <small class="text-muted">{{$item->Blank->code}}</small>@else <span class="text-danger">N/A</span> @endif</td>
                                        <th>
                                            @if($item->actionIn)
                                                <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{$item->actionIn}}</span>
                                            @endif
                                            @if($item->actionOut)
                                                <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{$item->actionOut}}</span>
                                            @endif
                                        </th>
                                        <td>{{$item->count}}</td>
                                        <td>{{$item->last_no}}</td>
                                        <td>{{$item->description}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                                <h3 class="mb-0">Last In/Out</h3>
                            </div>
                            <div class="col text-right">
                                <a href="{{ route('import.last') }}" class="btn btn-sm btn-primary">See all</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Product</th>
                                    <th scope="col">Serial</th>
                                    <th scope="col">In/Out</th>
                                    <th scope="col">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($history as $item)
                                    <tr>
                                        <th scope="row">{{$item->Product->code}}</th>
                                        <td><div class="d-flex align-items-center">{{$item->serial}}</div></td>
                                        <td><div class="d-flex align-items-center">{{$item->in_out}}</div></td>
                                        <td><div class="d-flex align-items-center">{{$item->date}}</div></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush