<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Items in Warehouse</h5>
                                    <span class="h2 font-weight-bold mb-0">{{ $itemsInWarehouse }}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-muted text-sm">
                                @if($itemsInWarehouse<$itemsInWarehouselastMonth)
                                    <span class="text-success mr-1"><i class="fa fa-arrow-up"></i>
                                @elseif($itemsInWarehouse>$itemsInWarehouselastMonth)
                                    <span class="text-danger mr-1"><i class="fa fa-arrow-down"></i>
                                @else
                                    <span class="mr-1">
                                @endif
                                {{$itemsInWarehouselastMonth}}</span>
                                <span class="text-nowrap">Last Month</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Total Blanks in Warehouse</h5>
                                    <span class="h2 font-weight-bold mb-0">{{ $blanksInWarehouse }}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-danger text-white rounded-circle shadow">
                                        <i class="fas fa-chart-bar"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-muted text-sm">
                                <span class="text-success mr-1">
                                    @if($blanksInWarehouselastMonth>0)<i class="fa fa-plus mr-1"></i>@endif
                                    {{$blanksInWarehouselastMonth}}
                                </span>
                                <span class="text-nowrap">This Month</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Today IN</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$countDayIN}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-green text-white rounded-circle shadow">
                                        <i class="fas fa-arrow-up"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-muted text-sm">
                                @if($countDayIN<$countINlastDay)
                                    <span class="text-success mr-1"><i class="fa fa-arrow-up"></i>
                                @elseif($countDayIN>$countINlastDay)
                                    <span class="text-danger mr-1"><i class="fa fa-arrow-down"></i>
                                @else
                                    <span class="mr-1">
                                @endif
                                {{$countINlastDay}}</span>
                                <span class="text-nowrap">Last Day</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Today Out</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$countDayOUT}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-yellow text-white rounded-circle shadow">
                                        <i class="fas fa-arrow-down"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-muted text-sm">
                                @if($countDayOUT<$countOUTlastDay)
                                    <span class="text-success mr-1"><i class="fa fa-arrow-up"></i>
                                @elseif($countDayOUT>$countOUTlastDay)
                                    <span class="text-danger mr-1"><i class="fa fa-arrow-down"></i>
                                @else
                                    <span class="mr-1">
                                @endif
                                {{$countOUTlastDay}}</span>
                                <span class="text-nowrap">Last Day</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Product Types</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$amountProductTypes}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-info text-white rounded-circle shadow">
                                        <i class="fas fa-inbox"></i>
                                    </div>
                                </div>
                            </div>
                            <p class="mt-3 mb-0 text-muted text-sm">
                                @if($amountProductTypesLast>0)
                                    <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> {{$amountProductTypesLast}}</span>
                                @elseif($amountProductTypesLast<0)
                                    <span class="text-danger mr-2"><i class="fa fa-arrow-down"></i> {{$amountProductTypesLast}}</span>
                                @else
                                    0
                                @endif
                                <span class="text-nowrap">Since Last Month</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>