@extends('layouts.app', ['title' => __('Add Product')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('Here you can add a new product.'),
        'class' => 'col-lg-7'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Add a New Product') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <form action="{!! route('product.add') !!}" method="post">
                            @csrf
                            @method('put')
                            <div class="pl-lg-4">
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="code" placeholder="product code" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="volume" placeholder="volume" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="manufacturer" placeholder="manufacturer" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="description" placeholder="description" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="text" name="material" placeholder="material" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="number" min="0" step="0.01" name="price_wholesale" placeholder="price wholesale" required />
                                </div>
                                <div class="form-group">
                                    <input class="form-control form-control-alternative" type="number" min="0" step="0.01" name="price_retail" placeholder="price retail" required />
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-info form-control-alternative" type="submit" value="Add">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection