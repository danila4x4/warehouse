@extends('layouts.app', ['title' => __('Last Imported Items')])

@section('content')
@include('users.partials.header', [
'title' => __('Hello') . ' '. auth()->user()->name,
'description' => __('This is for Last Imported Items. You can find all entries, which were imported in Data Base.'),
'class' => 'col-lg-12'
])

<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card bg-secondary shadow">
                <div class="card-header bg-white border-0">
                    <div class="row align-items-center">
                        <h3 class="col-12 mb-0">{{ __('Last Imported Items') }}</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Product</th>
                                <th>Serial</th>
                                <th>In/Out</th>
                                <th>Date</th>
                                <th>Created</th>
                                <th>Action</th>
                            </tr>
                            @foreach($warehouse_data as $item)
                                <tr>
                                    <td><a class="btn btn-outline-secondary border-0" href="{!! secure_url('product-details/'.$item->product_id) !!}">{!! $item->Product->code !!}</a></td>
                                    <td>{!! $item->serial !!}</td>
                                    <td>@if($item->in_out == 1) <span class="text-success">IN</span> @else <span class="text-info">OUT</span> @endif</td>
                                    <td>
                                        <b>{!! \Carbon\Carbon::parse($item->date)->format('d/m/Y') !!}</b>
                                    </td>
                                    <td>
                                        {!! \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i') !!}
                                    </td>
                                    <td><a class="btn btn-danger btn-sm" href="{!! secure_url('import-delete/'.$item->id) !!}" onclick="return confirm('DELETE!!! ARE YOU SURE?')" >Delete</a></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <div class="pagination">
                        {!! $warehouse_data->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@push('js')
<script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush

