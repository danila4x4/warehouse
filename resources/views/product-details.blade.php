@extends('layouts.app', ['title' => __('User Profile')])

@section('content')
    @include('users.partials.header', [
        'title' => __('Hello') . ' '. auth()->user()->name,
        'description' => __('The page for product. You can change/ add items.'),
        'class' => 'col-lg-12'
    ])
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-8 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0"><b>{{$productModel->code}}</b> <span class="text-muted">{{$productModel->description}}.</span> <span class="text-primary">Total amount of product: <b>{{$productModel->count}}</b></span> | Blanks: @if($productModel->Blank){{$productModel->Blank->count}} items @endif</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="row justify-content-center">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">Dashboard</div>
                                        <div class="card-body">
                                            <div class="alert alert-success" role="alert">
                                                {!! session('status') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if($serials)
                            <div class="row justify-content-center">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <th>Serial</th>
                                                        <th>IN <small>last Date</small></th>
                                                        <th>OUT <small>last Date</small></th>
                                                        <th>Duration</th>
                                                        <th>Total</th>
                                                    </tr>
                                                    @foreach($serials as $serial)
                                                        <tr>
                                                            <td>{!! $serial->serial !!}</td>
                                                            <td>{!! $serial->in !!}</td>
                                                            <td>{!! $serial->out !!}</td>
                                                            <td>{!! $serial->duration !!}</td>
                                                            <td><span class="@if($serial->count > 0) text-success @elseif(empty($serial->count)) text-muted @else text-danger @endif ">{!! $serial->count !!}</span></td>
                                                        </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xl-4 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Add In / Out product') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">

                        @if (session('statusProduct'))
                            <div class="alert alert-default alert-dismissible fade show" role="alert">
                                {!! session('statusProduct') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                        <form action="{!! route('import.warehouse-add') !!}" method="post">
                            {!! csrf_field() !!}
                            <div class="form-group">
                                <label for="product_id">Product</label>
                                <select class="form-control m-10" name="product_id" required>
                                    <option value="">Product</option>
                                    @foreach($products as $product)
                                        <option value="{!! $product->id !!}" selected>{!! $product->code !!}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="serial">Serial Number</label>
                                <input type="text" class="form-control m-10"  name="serial" placeholder="serial number" />
                            </div>
                            <div class="form-group">
                                <label for="in_out">IN or OUT</label>
                                <select class="form-control m-10" name="in_out" required>
                                    <option value="" disabled="disabled">choose IN or OUT</option>
                                    <option value="1">IN</option>
                                    <option value="0">OUT</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input type="date" class="form-control m-10"  name="date" value="{{ \Carbon\Carbon::today()->format('Y-m-d') }}" required />
                            </div>
                            <button type="submit" class="btn btn-outline-default">Check</button>
                        </form>

                        <div class="row justify-content-center my-4">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        Last Import for <b>{{$productModel->code}}</b>
                                        <div class="dropdown float-right">
                                            <a class="btn btn-sm btn-icon-only text-danger float-right" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                <a class="dropdown-item text-danger" href="{{ route('product.delete', $productModel->id) }}" onclick="return confirm_delete()" >You are going to delete this product and all data!!!</a>
                                            </div>
                                        </div>
                                        <div><a class="btn btn-success m-2" href="{!! route('product.export', $productModel->id) !!}">Download Excel</a></div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <th>Date</th>
                                                    <th>in/out</th>
                                                    <th>Serial</th>
                                                    <th>Action</th>
                                                </tr>
                                                @foreach($details as $detail)
                                                    <tr>
                                                        <td>{!! \Carbon\Carbon::parse($detail->date)->format('d/m/Y') !!}</td>
                                                        <td>@if($detail->in_out == 1) <span class="text-success">IN</span> @else <span class="text-info">OUT</span> @endif</td>
                                                        <td>@if($detail->serial){!! $detail->serial !!}@else n/a @endif</td>
                                                        <td><a class="btn btn-danger btn-sm" href="{!! url('import-delete/'.$detail->id) !!}" onclick="return confirm('DELETE!!! ARE YOU SURE?')"  >Delete</a></td>
                                                    </tr>
                                                @endforeach
                                            </table>
                                        </div>
                                        <div class="pagination">
                                            {{ $details->render() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@push('js')
    <script>
        function confirm_delete() {
            return confirm('This is the last action, after that - boom!!! Are you sure delete this Product?');
        }
    </script>
@endpush