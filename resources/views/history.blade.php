@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        @if (session('status'))
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Dashboard</div>
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Last imported positions</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Product</th>
                                    <th>Serial</th>
                                    <th>In/Out</th>
                                    <th>Date</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($warehouse_data as $item)
                                    <tr>
                                        <td><a class="btn btn-outline-secondary border-0" href="{!! secure_url('product-details/'.$item->product_id) !!}">{!! $item->Product->code !!}</a></td>
                                        <td>{!! $item->serial !!}</td>
                                        <td>@if($item->in_out == 1) <span class="text-success">IN</span> @else <span class="text-info">OUT</span> @endif</td>
                                        <td>
                                            <b>{!! \Carbon\Carbon::parse($item->date)->format('d/m/Y') !!}</b>
                                        </td>
                                        <td>
                                            {!! \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i') !!}
                                        </td>
                                        <td><a class="btn btn-danger btn-sm" href="{!! secure_url('import-delete/'.$item->id) !!}" onclick="return confirm('DELETE!!! ARE YOU SURE?')" >Delete</a></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination">
                            {!! $warehouse_data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">History Import Data</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>Date</th>
                                    <th>Data</th>
                                </tr>
                                @foreach($import_data as $item)
                                    <tr>
                                        <td>
                                            <b>{!! \Carbon\Carbon::parse($item->created_at)->format('d/m/Y h:i') !!}</b>
                                        </td>
                                        <td>{!! $item->data !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="pagination">
                            {!! $import_data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection