@extends('layouts.app', ['title' => __('Users')])

@section('content')
    @include('users.partials.header', [
    'title' => __('Hello') . ' '. auth()->user()->name,
    'description' => __('This is for all users. Can be added and edit user.'),
    'class' => 'col-lg-12'
    ])

    <div class="container-fluid">
        @if (session('status'))
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Dashboard</div>
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {!! session('status') !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Users</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table align-items-center">
                                <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Creation Date</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody class="list">
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{ $user->name }}</td>
                                        <td class="text-center"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></>
                                        <td class="text-center">{{ $user->created }}</td>
                                        <td class="text-center">
                                            <div class="dropdown">
                                                <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                                    @if(Auth()->user()->id == $user->id)
                                                        <a class="dropdown-item" href="{{ route('profile.edit') }}">Edit</a>
                                                    @else
                                                        <a class="dropdown-item" href="{{ route('profile.delete-user', $user->id) }}">Delete</a>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection