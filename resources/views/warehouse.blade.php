@extends('layouts.app', ['title' => __('Produxts')])

@section('content')
    @include('users.partials.header', [
    'title' => __('Hello') . ' '. auth()->user()->name,
    'description' => __('This is for all products. Can be added description, price and other information.'),
    'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Products') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form action="{{ secure_url('product-update') }}" method="post">
                                {{ csrf_field() }}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Blanks</th>
                                            <th>Updated</th>
                                            <th>Count</th>
                                            <th>Volume</th>
                                            <th>Manufacturer</th>
                                            <th>Description</th>
                                            <th>Material</th>
                                            <th>Wholesale</th>
                                            <th>Retail</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($products as $product)
                                            <tr @if($product->changed == 1) bgcolor="#FF0000" @endif >
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][code]" value="{{ $product->code }}" required /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][count]" value="{{ $product->Blank->count ?? 'N/A' }}" disabled/></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][updated_at]" value="{{ \Carbon\Carbon::parse($product->updated_at)->format('d/m/Y') }}" disabled /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][count]" value="{{ $product->count }}" disabled/></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][volume]" value="{{ $product->volume }}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][manufacturer]" value="{{ $product->manufacturer }}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][description]" value="{{ $product->description }}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{{ $product->id }}][material]" value="{{ $product->material }}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="number" min="0" step="0.01" name="data[{{ $product->id }}][price_wholesale]" value="{{ $product->price_wholesale }}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="number" min="0" step="0.01" name="data[{{ $product->id }}][price_retail]" value="{{ $product->price_retail }}" /></td>
                                                <td class="p-0"><a class="btn btn-secondary btn-sm" href="{{ secure_url('product-details/'.$product->id) }}" target="_blank">Details</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <input class="btn btn-info my-auto" type="submit" value="Update">
                                <input class="btn btn-success m-2" type="submit" name="download" value="Download Excel">
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection

@push('js')
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush


