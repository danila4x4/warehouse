@extends('layouts.app', ['title' => __('Blanks')])

@section('content')
    @include('users.partials.header', [
    'title' => __('Hello') . ' '. auth()->user()->name,
    'description' => __('This is for all blanks. Can be added count and edit other information.'),
    'class' => 'col-lg-12'
    ])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <h3 class="col-12 mb-0">{{ __('Blanks') }}</h3>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form action="{!! secure_url('blank-update') !!}" method="post">
                                {!! csrf_field() !!}
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Code</th>
                                            <th>Description</th>
                                            <th>Products</th>
                                            <th>Count</th>
                                            <th>Add Num</th>
                                            <th>Updated</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($blanks as $blank)
                                            <tr>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{!! $blank->id !!}][code]" value="{{$blank->code}}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{!! $blank->id !!}][description]" value="{{$blank->description}}" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{!! $blank->id !!}][products]" value="{{$blank->products_str}}" disabled/></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" id="count_{{$blank->id}}" type="number" min="0" step="1" name="data[{!! $blank->id !!}][count]" value="{{$blank->count}}" disabled/></td>
                                                <td class="p-0"><input class="form-control border-0 p-1 add_count" type="number" min="-10" step="1" name="data[{!! $blank->id !!}][add_count]" data-id="{{$blank->id}}" value="" /></td>
                                                <td class="p-0"><input class="form-control border-0 p-1" type="text" name="data[{!! $blank->id !!}][updated]" value="{{ $blank->updated }}" disabled /></td>
                                                <td class="p-0"><a class="btn btn-secondary btn-sm" href="{!! secure_url('blank-edit/'.$blank->id) !!}" target="_blank">Edit</a></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <input class="btn btn-info my-auto" type="submit" value="Update">
                                <input class="btn btn-success m-2" type="submit" name="download" value="Download Excel">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
    <script src="{{ secure_asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
@endpush