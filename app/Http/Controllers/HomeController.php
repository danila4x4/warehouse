<?php

namespace App\Http\Controllers;

use App\Models\BlankLogModel;
use App\Models\BlankModel;
use App\Models\ImportDataModel;
use App\Models\ProductModel;
use App\Models\WarehouseModel;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        //
        $countIN = WarehouseModel::where('in_out', '1')->count();
        $countOUT = WarehouseModel::where('in_out', '0')->count();
        $itemsInWarehouse = $countIN - $countOUT;

        $countINlastMonth = WarehouseModel::where('in_out', '1')->where('date', '<=', Carbon::today()->firstOfMonth())->count();
        $countOUTlastMonth = WarehouseModel::where('in_out', '0')->where('date', '<=', Carbon::today()->firstOfMonth())->count();
        $itemsInWarehouselastMonth = $countINlastMonth - $countOUTlastMonth;
//        $percentItems = $this->getPercent($itemsInWarehouse, $itemsInWarehouselastMonth);
        //

        //
        $amountProductTypes = ProductModel::count();
        $amountProductTypesLastCount = ProductModel::where('created_at', '<=', Carbon::today()->firstOfMonth())->count();
        $amountProductTypesLast = $amountProductTypes - $amountProductTypesLastCount;
        //

        //
        $blanksInWarehouse = BlankModel::sum('count');
        $blanksInWarehouselastMonth = BlankLogModel::where('created_at', '>=', Carbon::today()->firstOfMonth())
            ->where('add', '>', 1)->sum('add');
        //

        //
        $countDayIN = WarehouseModel::where('in_out', '1')->whereDate('date', Carbon::today())->count();
        $countDayOUT = WarehouseModel::where('in_out', '0')->whereDate('date', Carbon::today())->count();
        $countINlastDay = WarehouseModel::where('in_out', '1')->whereDate('date', Carbon::today()->subDay())->count();
        $countOUTlastDay = WarehouseModel::where('in_out', '0')->whereDate('date', Carbon::today()->subDay())->count();

        //
//        $percentItemsDayIn = $this->getPercent($countDayOUT, $countOUTlastDay);
//        $percentItemsDayOut = $this->getPercent($countDayIN, $countINlastDay);
        //

        $warehouseModel = WarehouseModel::with('Product')->orderby('created_at', 'desc')->limit(20)->get();
        $history = $warehouseModel->map(function($item) {
           $item->date = Carbon::parse($item->date)->format('d/m/y');
           $item->in_out = $item->in_out == 1 ? 'IN' : 'OUT';
           return $item;
        });

        $productModel = ProductModel::orderby('code', 'asc')->with('Blank', 'Warehouse')->get();
        $products = $productModel->Map(function ($item) {
            $item->actionIn = null;
            $item->actionOut = null;

            $item->actionIn = WarehouseModel::where('product_id', $item->id)->where('in_out', 1)->whereDate('date', '=', Carbon::today()->toDateString())->count();
            $item->actionOut = WarehouseModel::where('product_id', $item->id)->where('in_out', 0)->whereDate('date', '=', Carbon::today()->toDateString())->count();

            $warehouse = $item->Warehouse->sortByDesc('serial')->values()->first();
            $item->last_no = ($warehouse) ? $warehouse->serial : '-';
            return $item;
        });

        return view('dashboard', compact([
            'itemsInWarehouse',
            'itemsInWarehouselastMonth',
            'amountProductTypes',
            'amountProductTypesLast',
            'countDayIN',
            'countDayOUT',
            'countINlastDay',
            'countOUTlastDay',
            'history',
            'products',
            'blanksInWarehouse',
            'blanksInWarehouselastMonth'
        ]));
    }

    /**
     * Get Percent
     * @param $today
     * @param $last
     * @return false|float|int
     */
    private function getPercent($today, $last) {
        $result = 0;
        if (empty($today) && empty($last)) {
            $result = 0;
        } elseif(empty($today)) {
            $result = -100;
        } elseif(empty($last)) {
            $result = 100;
        } else {
            $result = round($today * 100 / $last, 1);
        }
        return $result;
    }
}
