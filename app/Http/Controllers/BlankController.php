<?php

namespace App\Http\Controllers;

use App\Exports\BlanksExport;
use App\Exports\ProductsExport;
use App\Models\BlankLogModel;
use App\Models\BlankModel;
use App\Models\ProductModel;
use Carbon\Carbon;
use http\Client\Response;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BlankController extends Controller
{
    // main view
    public function index() {
        $products = ProductModel::orderby('code', 'asc')->get();
        $blanks = BlankModel::orderby('code', 'asc')->with('Products')->get();
        $blanks = $blanks->map(function($item) {
            $products_array = null;
            foreach ($item->Products as $product) {
                $products_array[$product->id] = $product->code;
            }
            $item->products_str = $products_array ? implode(', ', $products_array) : 'n/a';
            $item->updated = Carbon::parse($item->updated_at)->format('d/m/Y');
            return $item;
        });
        return view('blank', compact(['products', 'blanks']));
    }

    // add a new blank
    public function add() {
        $products = ProductModel::orderby('code', 'asc')->get();
        $blank = $log = null;
        return view('blank-add', compact(['products', 'blank']));
    }

    public function edit($id) {
        $products = ProductModel::orderby('code', 'asc')->get();
        $blank = BlankModel::where('id', $id)->with('Products', 'Log')->first();
        return view('blank-add', compact(['products', 'blank']));
    }

    // store a new blank
    public function blankSubmit(Request $request) {
        $this->validate($request,[
            'code' => 'string|required',
            'description' => 'string|nullable',
            'count' => 'numeric|nullable',
            'products' => 'array',
            'blank_id' => 'numeric'
        ]);

        if($request->blank_id > 0) {
            $blankModel = BlankModel::where(['id' => $request->blank_id])->first();
        } else {
            $blankModel = BlankModel::firstOrNew(['code' => $request->code]);
        }
        $blankModel->fill(['description' => $request->description, 'count' => $request->count ? $request->count : 0]);
        $blankModel->save();

        foreach ($request->products as $product_id => $value) {
            $product = ProductModel::where('id', $product_id)->first();
            $product->blank_id = $blankModel->id;
            $product->save();
        }

        return redirect('blank');

    }

    public function blankDetails($id) {

    }

    public function blankUpdate(Request $request) {
        $this->validate($request,['data' => 'array']);

        if ($request->download) {return $this->export();}

        foreach ($request->data as $id => $item) {
            // store blank
            $blankModel = BlankModel::where('id', $id)->first();
            $blankModel->fill([
                'code' => $item['code'],
                'description' => $item['description']
            ]);
            if ($item['add_count'] > 0 || $item['add_count'] < 0) {
                $blankModel->count = $blankModel->count + $item['add_count'];
            }
            $blankModel->save();

        }

        return redirect()->back();
    }

    public function export()
    {
        return Excel::download(new BlanksExport(), 'blanks.xlsx');
    }

    public function blankProductUpdate(Request $request)
    {
        $this->validate($request,['product_id' => 'numeric|required', 'blank_id' => 'numeric|required', 'value' => 'nullable|numeric']);
        $value = $request->value == 1 ? $request->blank_id : null;
        $productModel = ProductModel::where('id', $request->product_id)->first();
        $productModel->blank_id = $value;
        $productModel->save();
        return response()->json(['status' => 'true', 'value' => $request->value, 'blank_id' => $request->blank_id]);

    }

    public function blankDelete($blank_id) {
        $blank = BlankModel::where('id', $blank_id)->first();
        $blank->Log->isNotEmpty() ? $blank->Log->each->delete() : '';
        $blank->delete();
        return redirect()->back();
    }
}