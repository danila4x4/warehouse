<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        $userModel = User::orderby('name', 'asc')->get();
        $users = $userModel->map(function($item) {

            $item->created = Carbon::parse($item->created_at)->format('m/d/Y');

            return $item;

        });


        return view('users.index', compact(['users']));
    }
}
