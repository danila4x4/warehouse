<?php

namespace App\Http\Controllers;

use App\Exports\DetailsExport;
use App\Exports\ProductsExport;
use App\Models\ProductModel;
use App\Models\WarehouseModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ProductController extends Controller
{
    protected $changed;

    public function productDetails($product_id) {

        $productModel = ProductModel::where('id', $product_id)->with('Blank')->first();
        $product_name = $productModel->code;
        $product_desc = $productModel->description;
        $count = $productModel->count;
        $products = ProductModel::orderby('code', 'asc')->where('id', $product_id)->with('Blank')->get();

        $details = WarehouseModel::where('product_id', $product_id)->with('Product')->orderby('date', 'desc')->Paginate(30);

        $serialModel = WarehouseModel::where('product_id', $product_id)->orderby('serial', 'asc')->distinct()->pluck('serial');

        $serials = null;

        foreach ($serialModel as $serial) {

            $in = WarehouseModel::where('product_id', $product_id)->where('serial', $serial)->where('in_out', 1)->latest('date')->value('date');
            $out = WarehouseModel::where('product_id', $product_id)->where('serial', $serial)->where('in_out', 0)->latest('date')->value('date');

            $count_in = WarehouseModel::where('product_id', $product_id)->where('serial', $serial)->where('in_out', 1)->count();
            $count_out = WarehouseModel::where('product_id', $product_id)->where('serial', $serial)->where('in_out', 0)->count();

            $serials[] = (object) [
                'serial' => $serial ? $serial : 'n/a',
                'in' => $in ? Carbon::parse($in)->format('d/m/Y') : 'n/a',
                'out' => $out ? Carbon::parse($out)->format('d/m/Y') : '',
                'duration' => $in && $out ? Carbon::parse($out)->diffForHumans(Carbon::parse($in)) : '',
                'count' => $count_in - $count_out,
            ];
        }

        if ($this->changed) {
            $products = $products->map(function ($product) {
                if (in_array($product['id'], $this->changed)) {
                    $product['changed'] = 1;
                }
                return $product;
            });
        }
        return view('product-details', compact(['productModel', 'details',
            'serials', 'products']));
    }

    public function productAddIndex() {
        return view('product-add');
    }

    public function productAdd(Request $request) {

        $this->validate($request,[
            'code' => 'string|required',
            'manufacturer' => 'string|required',
            'description' => 'string|required',
            'material' => 'string|required',
            'volume' => 'string|required',
            'price_wholesale' => 'numeric|required',
            'price_retail' => 'numeric|required'
        ]);

        ProductModel::create([
            "code" => $request->code,
            "manufacturer" => $request->manufacturer,
            "description" => $request->description,
            "material" => $request->material,
            "volume" => $request->volume,
            "price_wholesale" => $request->price_wholesale,
            "price_retail" => $request->price_retail
        ]);

        return redirect('warehouse');
    }

    public function productUpdate(Request $request) {

        $this->validate($request,[
            'data' => 'required'
        ]);

        if ($request->download) {
            return $this->export();
        }

        $products = $request->data;

        foreach ($products as $product_id => $value) {
            $prodModel = ProductModel::find($product_id);
            $prodModel->fill([
                "code" => $value['code'],
                "manufacturer" => $value['manufacturer'],
                "description" => $value['description'],
                "material" => $value['material'],
                "volume" => $value['volume'],
                "price_wholesale" => $value['price_wholesale'],
                "price_retail" => $value['price_retail'],
//                "count" => $value['count'],
            ]);
            $prodModel->save();
        }

        return redirect('warehouse');

    }

    public function export()
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    public function exportDetails($product_id)
    {
        return Excel::download(new DetailsExport($product_id), 'details.xlsx');
    }

    public function productDelete($product_id)
    {
        $productModel = ProductModel::where('id', $product_id)->first();
        $productModel->Warehouse->each->delete();
        $productModel->delete();
        return redirect(route('product.all'));
    }

}
