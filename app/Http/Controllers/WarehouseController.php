<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Exports\DetailsExport;
use App\Exports\WarehouseExport;
use App\Models\ImportDataModel;
use App\Models\ImportIdModel;
use App\Models\ProductModel;
use App\Models\WarehouseModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class WarehouseController extends Controller
{

    protected $changed;
    protected $errorData;

    public function index() {

        $products = ProductModel::orderby('code', 'asc')->get();
        if ($this->changed) {
            $products = $products->map(function ($product) {
                if (in_array($product->id, $this->changed)) {
                    $product->changed = 1;
                }
                return $product;
            });
        }
        return view('warehouse', compact(['products']));
    }

    public function indexImport() {
        $products = ProductModel::orderby('code', 'asc')->get();
        $warehouse_data = WarehouseModel::with(['Product'])->orderby('id', 'desc')
            ->Paginate(30);
        return view('import', compact(['products', 'warehouse_data']));
    }

    public function confirm(Request $request) {

        $this->validate($request,[
            'data' => 'required'
        ]);

        $confirm = $this->insertIntoWarehouse($request->data, 'confirm');
        $data = '';
        $products = '';
        $import_data = '';

        return view('import-check', compact(['data', 'products', 'import_data', 'confirm']));

    }

    public function import(Request $request) {
        $this->validate($request,[
            'data' => 'required'
        ]);

        $import = New ImportIdModel();
        $import->save();

        $new = ImportDataModel::firstOrCreate(['import_id' => $import->id, 'data' => $request->data]);
        $new->save();

        $this->insertIntoWarehouse($request->data,  'insert');

        $productModel = New ProductModel();
        $result = $productModel->countProducts();
        $this->changed = isset($result['changed']) ? $result['changed'] : '';

        return redirect('/')->with('status', 'Import was comleted!');

    }

    public function insertIntoWarehouse($data, $type) {

        if (Str::contains(strtolower($data), "\r\n")) {
            $array = explode("\r\n", $data);
        } elseif (Str::contains(strtolower($data), "\r")) {
            $array = explode("\r", $data);
        } elseif (Str::contains(strtolower($data), "\n")) {
            $array = explode("\n", $data);
        } else {
            $array = explode(" ", $data);
        }

        $in_out = 1;
        $date = Carbon::today();
        $confirm = null;
        $this->errorData = null;

        $result_array = null;

        foreach ($array as $key => $item) {

            $message = null;

            if (Str::contains(strtolower($item), ['begin', '----', 'total', 'count'])) {
                $this->errorData[] = $item;
                continue;
            }

            if (Str::contains(strtolower($item), 'out')) {
                $in_out = 0;
                if (strlen($item) > 4 && Str::contains('-', $item)) {
                    $ar = explode('-', $item);
                    if (isset($ar[1])) {
                        $date = Carbon::parse($ar[1])->toDateString();
                    }
                }
            } elseif (Str::contains(strtolower($item), 'in')) {
                $in_out = 1;
                if (strlen($item) > 3 && Str::contains('-', $item)) {
                    $ar = explode('-', $item);
                    if (isset($ar[1])) {
                        $date = Carbon::parse($ar[1])->toDateString();
                    }
                }
            } elseif (strlen($item) == 10 && preg_match("/[0-9]{2}.[0-9]{2}.[0-9]{4}/", $item)) {
                $date = Carbon::parse($item)->toDateString();
            } else {

                if(preg_match("/-\z/i", $item)) {
                    $product_code = rtrim($item, "-");
                    $product_serial = $array[$key+1];
                    $item = $product_code . '-' . $product_serial;
                } else {
                    $product_array = explode('-', $item);
                    $product_code = isset($product_array[0]) ? $product_array[0] : null;
                    $product_serial = isset($product_array[1]) ? $product_array[1] : null;
                }

                if (empty($product_code)) {
                    $this->errorData[] = $item;
                    continue;
                }

                // get all products in one array
                $product = ProductModel::where(['code' => $product_code])->with('Blank')->first();
                if (empty($product)) {
                    $this->errorData[] = $item;
                    continue;
                }
                $code = $product->code;
                $product_id = $product->id;
                $blanks = $product->Blank ? $product->Blank->count : 'n/a';

                // check product and display message
                $get_message = $this->checkProductSerial($in_out, $product, $product_serial);
                $message = $get_message['message'];
                $result = $get_message['status'];

                $result_array[] = [
                    'date' => $date,
                    'product_id' => $product_id,
                    'product' => $item,
                    'code' => $code,
                    'serial' => $product_serial,
                    'in_out' => $in_out,
                    'message' => $message,
                    'result' => $result,
                    'blanks' => $blanks
                ];
            }
        }

        $confirm = null;
        if ($type == 'insert') {

            foreach ($result_array as $item) {

                if ($item['result'] === false) {
                    $continue = true;
                    foreach ($result_array as $check) {
                        if ($item['code'] == $check['code'] && $item['serial'] == $check['serial'] && $item['in_out'] != $check['in_out']) {
                            $continue = false;
                            break;
                        }
                    }
                    if ($continue) { continue; }
                }

                $new = New WarehouseModel();
                $new->product_id = $item['product_id'];
                $new->serial = $item['serial'];
                $new->product = $item['product'];
                $new->in_out = $item['in_out'];
                $new->date = $item['date'];
                $new->save();

            }

        } elseif($type == 'confirm') {

            foreach ($result_array as $item) {

                if ($item['result'] === false) {
                    foreach ($result_array as $check) {
                        if ($item['code'] == $check['code'] && $item['serial'] == $check['serial'] && $item['in_out'] != $check['in_out']) {
                            $item['message'] = 'OK';
                            break;
                        }
                    }
                }
                $confirm[] = [
                    'data' => $item['date'],
                    'code' => $item['code'],
                    'serial' => $item['serial'],
                    'in_out' => $item['in_out'] == 1 ? '<span class="text-success">IN</span>' : '<span class="text-info">OUT</span>',
                    'message' => $item['message'],
                    'blanks' => $item['blanks']
                ];
            }
        }

        return ['confirm' => $confirm, 'data' => $data, 'errorData' => $this->errorData];

    }

    public function warehouseAdd(Request $request) {
        $this->validate($request,[
            'product_id' => 'required',
            'in_out' => 'required',
            'date' => 'required',
            'serial' => ''
        ]);

        $productModel = ProductModel::where('id', $request->product_id)->first();

        // check product and display message
        $in_out = $request->in_out == 1 ? 'Add' : 'Out';

        $get_message = $this->checkProductSerial($request->in_out, $productModel, $request->serial)['message'];

        $message = $in_out . ' product ' . $productModel->code .'-'. $request->serial .
            '<br/>message: ' . $get_message;

        $warehouseModel = New WarehouseModel();
        $warehouseModel->fill([
            "product_id" => $request->product_id,
            "serial" => $request->serial,
            "product" => $productModel->code,
            "in_out" => $request->in_out,
            "date" => $request->date,
        ]);
        $warehouseModel->save();

        $productModel = New ProductModel();
        $result_count = $productModel->countProducts($request->product_id);

        $count = isset($result_count['count'][$request->product_id]) ? $result_count['count'][$request->product_id] : 'n/a';

        $link = '<a href="'.secure_url('product-details/' . $request->product_id).'" target="_blank">Open Product</a>';

        $message = $message .
                    '<br/>Total count: ' . $count . ' items' .
                    '<br/>' . $link;

        return redirect()->back()->with(['statusProduct' => $message]);

    }

    public function historyImport() {
        $import_data = ImportDataModel::orderby('id', 'desc')->Paginate(30);
        return view('imported-data', compact(['import_data']));
    }

    public function lastImport() {
        $warehouse_data = WarehouseModel::with(['Product'])->orderby('id', 'desc')
            ->Paginate(30);
        return view('imported-last', compact(['warehouse_data']));
    }

    public function deleteImport($id) {
        $data = WarehouseModel::findOrFail($id);
        $product = $data->product;
        $product_id = $data->product_id;
        $data->delete();
        $productModel = New ProductModel();
        $productModel->countProducts($product_id);
        return redirect()->back()->with('status', $product . ' was deleted');
    }

    private function checkProductSerial($in_out, $product, $product_serial) {

        $message = '<span class="text-success">Ok</span>';
        $result = true;

        if ($in_out == 0) {
            $check_in = WarehouseModel::where(['product_id' => $product->id, 'in_out' => 1, 'serial' => $product_serial])
                ->count();
            $check_out = WarehouseModel::where(['product_id' => $product->id, 'in_out' => 0, 'serial' => $product_serial])
                ->count();

            if (empty($check_out) && $check_in < 1) {
                $message = '<span class="text-danger">No product.</span>';
                $result = false;
            } elseif ($check_out && $check_in && ($check_in == $check_out)) {
                $message = '<span class="text-danger">No product. The product has been IN and then was OUT.</span>';
                $result = false;
            } elseif ($check_in < $check_out) {
                $message = '<span class="text-danger">No product.</span>';
                $result = false;
            }
        } elseif($in_out == 1) {
            $check_in = WarehouseModel::where(['product_id' => $product->id, 'in_out' => 1, 'serial' => $product_serial])
                ->count();
            $check_out = WarehouseModel::where(['product_id' => $product->id, 'in_out' => 0, 'serial' => $product_serial])
                ->count();
            if ($check_in > $check_out) {
                $message = '<span class="text-info">This product with this serial number is IN.</span>';
                $result = false;
            } elseif(empty($check_in) && $check_out) {
                $message = '<span class="text-info">This product with this serial number was OUT, but on IN.</span>';
                $result = false;
            }
            // check blanks. Can not be 0
            if (empty($product->Blank)) {
                $message = '<span class="text-danger">No blank.</span>';
                $result = false;
            } elseif ($product->Blank && $product->Blank->count < 1) {
                $message = '<span class="text-danger">No blank.</span>';
                $result = false;
            }
        }

        return ['message' => $message,'status' => $result];
    }

}
