<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WarehouseModel extends Model
{
    protected $table="warehouse";
    protected $fillable=["product_id", "serial", "product", "in_out", "date"];
    public $dates = ['created_at', 'updated_at'];

    public function Product() {
        return $this->belongsTo(ProductModel::class, "product_id");
    }





}
