<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductModel extends Model
{
    protected $changed;

    protected $table="products";
    protected $fillable=["code", "manufacturer", "description", "material", "volume",
        "price_wholesale", "price_retail", "count", "blank_id", "count"];
    public $dates = ['created_at', 'updated_at'];

    public function Warehouse() {
        return $this->hasMany(WarehouseModel::class, 'product_id');
    }

    public function Blank() {
        return $this->belongsTo(BlankModel::class, 'blank_id');
    }

    public function countProducts($product_id = null) {

        $count_result = null;

        $products = ProductModel::where(function($q) use ($product_id) {
            if ($product_id) {
                $q->where('id', $product_id);
            }
        })->get();

        foreach ($products as $product) {
            $in = WarehouseModel::where(['product_id' => $product->id, 'in_out' => 0])->count();
            $out = WarehouseModel::where(['product_id' => $product->id, 'in_out' => 1])->count();
            $count = $out - $in;
            $product->count = $count;
            if ($product->isDirty()) {
                $this->changed[] = $product->id;
            }
            $product->save();
            $count_result[$product->id] = $count;
        }

        return ['count' => $count_result, 'changed' => $this->changed];

    }

}
