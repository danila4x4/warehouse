<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlankLogModel extends Model
{
    protected $table="blank_log";
    protected $fillable=["blank_id", "add"];
    public $dates = ['created_at', 'updated_at'];

    public function Blank() {
        return $this->belongsTo(BlankModel::class,'blank_id');
    }
}
