<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportIdModel extends Model
{
    protected $table="import_id";
    public $dates = ['created_at', 'updated_at'];
}
