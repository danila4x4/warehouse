<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlankModel extends Model
{
    protected $table="blank";
    protected $fillable=["code", "description", "count"];
    public $dates = ['created_at', 'updated_at'];

    public function Products() {
        return $this->hasMany(ProductModel::class,'blank_id');
    }

    public function Log() {
        return $this->hasMany(BlankLogModel::class, 'blank_id');
    }
}
