<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImportDataModel extends Model
{
    protected $table="import_data";
    protected $fillable=["import_id", "data"];
    public $dates = ['created_at', 'updated_at'];
}
