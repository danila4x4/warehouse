<?php

namespace App\Exports;

use App\WarehouseModel;
use Maatwebsite\Excel\Concerns\FromCollection;

class WarehouseExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return \App\Models\WarehouseModel::all();
    }
}
