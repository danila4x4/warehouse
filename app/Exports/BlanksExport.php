<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BlanksExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return \App\Models\BlankModel::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'code',
            'description',
            'count',
            'created_at',
            'updated_at'
        ];
    }
}