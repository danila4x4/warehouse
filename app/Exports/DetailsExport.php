<?php

namespace App\Exports;

use App\Productmodel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DetailsExport implements FromCollection, WithHeadings
{

    protected $product_id;

    public function __construct($product_id){
        $this->product_id = $product_id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return \App\Models\WarehouseModel::where('product_id', $this->product_id)->get();
    }

    public function headings(): array
    {
        return [
            "#",
            "product_id",
            "serial",
            "product",
            "in_out",
            "date",
            "created_at",
            "updated_at"
        ];
    }

}