<?php

namespace App\Exports;

use App\Productmodel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return \App\Models\ProductModel::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'code',
            'manufacturer',
            'description',
            'material',
            'volume',
            'price_wholesale',
            'price_retail',
            'count',
            'created_at',
            'updated_at'
        ];
    }

}
