<?php

namespace App\Observers;

use App\Models\ProductModel;

class ProductObserver
{
    public function deleting(ProductModel $product) {
        $product->Warehouse->isNotEmpty() ? $product->Warehouse->each->delete() : '';
    }
}
