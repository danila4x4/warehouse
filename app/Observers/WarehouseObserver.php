<?php

namespace App\Observers;

use App\Models\BlankLogModel;
use App\Models\BlankModel;
use App\Models\WarehouseModel;

class WarehouseObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  $warehouse
     * @return void
     */
    public function created(WarehouseModel $warehouse)
    {
        if ($warehouse->in_out == 1) {
            // get blank_id
            $blank_id = $warehouse->Product->blank_id;
            // decrease total
            $blankModel = BlankModel::where('id', $blank_id)->first();
            $blankModel->count = $blankModel->count - 1;
            $blankModel->save();
            // store log
            $logModel = new BlankLogModel();
            $logModel->fill(['blank_id' => $blank_id, 'add' => '-1']);
            $logModel->save();
        }
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  $warehouse
     * @return void
     */
    public function updated(WarehouseModel $warehouse)
    {

    }
}
