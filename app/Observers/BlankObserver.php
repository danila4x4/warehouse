<?php

namespace App\Observers;

use App\Models\BlankLogModel;
use App\Models\BlankModel;

class BlankObserver
{
    /**
     * Handle the task "created" event.
     *
     * @param  $blank
     * @return void
     */
    public function created(BlankModel $blank)
    {
        if ($blank->count > 0) {
            $blankLogModel = new BlankLogModel();
            $blankLogModel->fill(['blank_id' => $blank->id, 'add' => $blank->count]);
            $blankLogModel->save();
        }
    }

    /**
     * Handle the task "updated" event.
     *
     * @param  $blank
     * @return void
     */
    public function updated(BlankModel $blank)
    {
        if($blank->isDirty('count')){
            // count has changed
            $add = $blank->count - $blank->getOriginal('count');
            if ($blank->count > 0) {
                $blankLogModel = new BlankLogModel();
                $blankLogModel->fill(['blank_id' => $blank->id, 'add' => $add]);
                $blankLogModel->save();
            }
        }
    }

    /**
     * Handle the task "deleted" event.
     *
     * @param  $blank
     * @return void
     */
    public function deleting(BlankModel $blank)
    {
        if ($blank->Products->isNotEmpty()) {
            foreach ($blank->Products as $product) {
                $product->blank_id = null;
                $product->save();
            }
        }

        $blank->Log->isNotEmpty() ? $blank->Log->each->delete() : '';

    }

    /**
     * Handle the task "restored" event.
     *
     * @param  $blank
     * @return void
     */
    public function restored(BlankModel $blank)
    {
        //
    }

    /**
     * Handle the task "force deleted" event.
     *
     * @param  $blank
     * @return void
     */
    public function forceDeleted(BlankModel $blank)
    {
        //
    }
}
