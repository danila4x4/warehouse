<?php

namespace App\Providers;

use App\Models\BlankModel;
use App\Models\ProductModel;
use App\Models\WarehouseModel;
use App\Observers\BlankObserver;
use App\Observers\ProductObserver;
use App\Observers\WarehouseObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        \Illuminate\Support\Facades\URL::forceScheme('https');
        BlankModel::observe(BlankObserver::class);
        WarehouseModel::observe(WarehouseObserver::class);
        ProductModel::observe(ProductObserver::class);
    }
}
