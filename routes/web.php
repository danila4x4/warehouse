<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
//
//Route::group(['middleware' => ['auth']], function () {
//
//    Route::redirect('/home', '/', 301);
//
//    Route::get('/', 'WarehouseController@index')->name('home');
//
//    Route::post('confirm', 'WarehouseController@confirm');
//    Route::post('import', 'WarehouseController@import');
//    Route::get('import-delete/{id}', 'WarehouseController@deleteImport');
//
//    Route::post('warehouse-add', 'WarehouseController@warehouseAdd');
//
//    Route::get('export', 'ProductController@export');
//    Route::get('export-details/{id}', 'ProductController@exportDetails');
//
//    Route::post('product-update', 'ProductController@productUpdate');
//    Route::post('product-add', 'ProductController@productAdd');
//
//    Route::get('product-details/{id}', 'ProductController@productDetails');
//    Route::get('history-import', 'WarehouseController@historyImport');
//
//});


Auth::routes();

Route::redirect('/home', '/', 301);

Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);

    Route::get('delete-user/{id}', ['as' => 'profile.delete-user', 'uses' => 'ProfileController@delete']);

    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    Route::get('add', ['as' => 'profile.add', 'uses' => 'ProfileController@add']);
    Route::put('add', ['as' => 'profile.store', 'uses' => 'ProfileController@store']);

    Route::get('import', ['as' => 'import.data', 'uses' => 'WarehouseController@indexImport']);
    Route::post('confirm', ['as' => 'import.confirm', 'uses' => 'WarehouseController@confirm']);
    Route::post('store', ['as' => 'import.store', 'uses' => 'WarehouseController@import']);
    Route::post('warehouse-add', ['as' => 'import.warehouse-add', 'uses' => 'WarehouseController@warehouseAdd']);
    Route::get('import-delete/{id}', ['as' => 'import.delete', 'uses' => 'WarehouseController@deleteImport']);
    Route::get('imported-data', ['as' => 'import.history', 'uses' => 'WarehouseController@historyImport']);
    Route::get('imported-last', ['as' => 'import.last', 'uses' => 'WarehouseController@lastImport']);

    Route::get('warehouse', ['as' => 'warehouse', 'uses' => 'WarehouseController@index']);

    Route::get('blank', ['as' => 'blank', 'uses' => 'BlankController@index']);
    Route::get('blank-add', ['as' => 'blank.add', 'uses' => 'BlankController@add']);
    Route::get('blank-edit/{id}', ['as' => 'blank.add', 'uses' => 'BlankController@edit']);
    Route::put('blank-add', ['as' => 'blank.add', 'uses' => 'BlankController@blankSubmit']);
    Route::get('blank-details', ['as' => 'blank.details', 'uses' => 'BlankController@blankDetails']);
    Route::post('blank-update', ['as' => 'blank.update', 'uses' => 'BlankController@blankUpdate']);
    Route::post('ajax-blank-product', ['as' => 'blank.product', 'uses' => 'BlankController@blankProductUpdate']);
    Route::get('blank-delete/{id}', ['as' => 'blank.delete', 'uses' => 'BlankController@blankDelete']);

    Route::get('products', ['as' => 'product.all', 'uses' => 'WarehouseController@index']);
    Route::get('product-details/{id}', ['as' => 'product.details', 'uses' => 'ProductController@productDetails']);
    Route::get('export-details/{id}', ['as' => 'product.export', 'uses' => 'ProductController@exportDetails']);
    Route::put('product-add', ['as' => 'product.add', 'uses' => 'ProductController@productAdd']);
    Route::get('product-add', ['as' => 'product.add', 'uses' => 'ProductController@productAddIndex']);
    Route::post('product-update', ['as' => 'product.update', 'uses' => 'ProductController@productUpdate']);
    Route::get('product-delete/{id}', ['as' => 'product.delete', 'uses' => 'ProductController@productDelete']);


});

