<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlankLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blank_log', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('blank_id');
            $table->integer('add');
            $table->timestamps();

            $table->foreign('blank_id')->on('blank')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blank_log');
    }
}
