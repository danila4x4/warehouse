<?php

use Illuminate\Database\Seeder;

class BlankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Ps1', 'Ps2', 'S2', 'L3p', 'L3d', 'El3', 'S3p', 'S3d', 'Y6'];

        foreach ($data as $code) {
            \App\Models\BlankModel::firstOrCreate(['code' => $code]);
        }
    }
}
